//
//  Renderer.cpp
//  SpaceDiner
//
//  Created by Martin Grant on 04/12/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#include "Renderer.hpp"

Renderer::Renderer(SDL_Window *window)
{
	m_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	
	int flags = IMG_INIT_JPG|IMG_INIT_PNG;
	IMG_Init(flags);
	
	m_textureMap["test"] = IMG_LoadTexture(m_renderer, "test2.png");
	m_textureMap["tile"] = IMG_LoadTexture(m_renderer, "tile.png");
	m_textureMap["wall"] = IMG_LoadTexture(m_renderer, "wall");
	m_textureMap["background"] = IMG_LoadTexture(m_renderer, "background.png");
	
	SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 100);
	SDL_SetRenderDrawBlendMode(m_renderer, SDL_BLENDMODE_BLEND);
	
	if (TTF_Init() == -1) {
		printf("TTF_Init: %s\n", TTF_GetError());
	}
	
	font = TTF_OpenFont("manteka.ttf", 32);
	if (!font)
	{
		printf("TTF_OpenFont: %s\n", TTF_GetError());
	}

}


Renderer::~Renderer()
{
	TTF_CloseFont(font);
	TTF_Quit();
	
	SDL_DestroyTexture(bitmapTex);
	
	SDL_DestroyRenderer(m_renderer);
}


void Renderer::renderBegin()
{
	SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 100);
	SDL_RenderClear(m_renderer);
}


void Renderer::renderEnd()
{
	SDL_RenderPresent(m_renderer);	
}


void Renderer::render(Entity &entity)
{
	SDL_RenderCopy(m_renderer, m_textureMap[entity.getTextureName()], NULL, &entity.getRect());
	
	if (entity.hasInteract() == true)
	{
		renderBar(entity.getRect().x, entity.getRect().y - 20, entity.getInteractBarWidth(), 10);
	}
}


void Renderer::render(std::string texture, int x, int y, int width, int height)
{
	SDL_Rect rect;
	rect.x = x;
	rect.y = y;
	rect.w = width;
	rect.h = height;
	SDL_RenderCopy(m_renderer, m_textureMap[texture], NULL, &rect);
}


void Renderer::renderTest(int x, int y, int type, std::string texture)
{
	
	
	SDL_Rect rect;
	rect.x = x;
	rect.y = y;
	rect.w = 40;
	rect.h = 40;
	
	//render(texture, x, y, 40, 40);
	
	// path
	if (type == 1)
		SDL_SetRenderDrawColor(m_renderer, 0, 255, 0, 100);
	
	// free
	if (type == 0)
		SDL_SetRenderDrawColor(m_renderer, 255, 255, 255, 100);
	
	// wall
	if (type == 9)
	{
		//render("tile", x, y, 40, 40);
		SDL_SetRenderDrawColor(m_renderer, 255, 100, 0, 100);
	}
	
	
	
	SDL_RenderDrawRect(m_renderer, &rect);
	SDL_RenderFillRect(m_renderer, &rect);
	
	
}


void Renderer::renderBar(int x, int y, int width, int height)
{
	SDL_Rect rect;
	rect.x = x;
	rect.y = y;
	rect.w = width;
	rect.h = height;
	SDL_SetRenderDrawColor(m_renderer, 255, 0, 0, 100);
	SDL_RenderDrawRect(m_renderer, &rect);
	SDL_RenderFillRect(m_renderer, &rect);
}


void Renderer::renderText(int x, int y, std::string text, int alpha)
{
	SDL_Color color = {255,0,0};
	
	SDL_Surface *textSurface = TTF_RenderText_Solid(font, text.c_str(), color);
	SDL_Texture *textTexture = SDL_CreateTextureFromSurface(m_renderer, textSurface);
		
	SDL_Rect textRect;
	textRect.x = x;
	textRect.y = y;
	textRect.w = textSurface->w;
	textRect.h = textSurface->h;
	
	SDL_SetTextureBlendMode(textTexture, SDL_BLENDMODE_BLEND);
	SDL_SetTextureAlphaMod(textTexture, alpha);
	
	SDL_RenderCopy(m_renderer, textTexture, NULL, &textRect);
	
	SDL_FreeSurface(textSurface);
	SDL_DestroyTexture(textTexture);
}
