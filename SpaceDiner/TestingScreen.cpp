#include "TestingScreen.h"


TestingScreen::TestingScreen(ScreenManagerInterface *screenManager, Renderer *renderer)
{
	m_player = Entity("test", 0.0f, 0.0f, 40.0f, 40.0f, true, false);
	m_test = Entity("test", 400.0f, 200.0f, 40.0f, 40.0f, false, false);
	object = Entity("tile", 400.0f, 300.0f, 100.0f, 100.0f, false, true);
	
	
	m_screenManager = screenManager;
	m_screenManager->changeScreen("intro");
	
	m_renderer = renderer;
	
	m_input = new SDLInput(0);
	
	m_level = new Level(13, 13);
	std::vector<int> levelvector = m_level->getLevelVector();
	std::copy(std::begin(levelvector), std::end(levelvector), std::begin(world_map));
	
	
	std::vector<std::pair<int,int>> coords;
	for (int i = 0; i < m_level->getWidth(); ++i)
	{
		for (int j = 0; j < m_level->getHeight(); ++j)
		{
			coords.push_back(std::make_pair(j * 40, i * 40));
		}
	}
	
	for (int i = 0; i < 40; ++i)
	{
		if (levelvector[i] == 9)
		{
			levelentities.push_back(Entity("wall", coords[i].first, coords[i].second, 40.0f, 40.0f, false, false));
		}
	}
	
	//levelentities.push_back(Entity("wall", 0, 0, 40.0f, 40.0f, false, false));
	//levelentities.push_back(Entity("wall", 40, 40, 40.0f, 40.0f, false, false));
}


TestingScreen::~TestingScreen()
{
	delete m_level;
}


void TestingScreen::update()
{
	m_player.update();
	object.update();
	m_test.update();
	response();
	
	if (m_input->getKeyState("e"))
	{
		if (collideInteractable() == true)
		{
			object.setInteracting(true);
		}
	}
	else
	{
		object.setInteracting(false);
	}
	
	if (m_input->getKeyState("g"))
	{
		path = m_level->findPath();
		m_test.setpath(path);
		//m_test.setpath(m_level->findPath());
		
		
	}
}


void TestingScreen::render()
{
	/*for (int i = 0; i <= 840; i+= 70)
	{
		for (int j = 0; j <= 630; j+= 70)
		{
			m_renderer->render("background", i, j, 70, 70);
		}
	}*/
	
	for (int i = 0; i < m_level->getWidth(); ++i)
	{
		for (int j = 0; j < m_level->getHeight(); ++j)
		{
			// render grid
			m_renderer->renderTest(j * 40, i * 40, m_level->getLevel()[i][j].getType(), m_level->getLevel()[i][j].getTexture());
			
			// render a* path
			if (path.empty() == false)
			{
				for (std::vector<std::pair<int,int>>::iterator it = path.begin(); it != path.end(); ++it)
				{
					if (it->first == j && it->second == i)
					{
						m_renderer->renderTest(j*40, i*40, 1, "test2");
					}
				}
			}
		}
	}
	
	m_renderer->render(m_test);
	m_renderer->render(m_player);
	
	//m_renderer->render(object);
	
	m_renderer->renderText(100, 400, "Hello world!", 100);
}


float TestingScreen::SweptAABB(Entity b1, Entity b2, float& normalx, float& normaly)
{
	float xInvEntry, yInvEntry;
	float xInvExit, yInvExit;
	
	// find the distance between the objects on the near and far sides for both x and y
	if (b1.getVelocity().x > 0.0f)
	{
		xInvEntry = b2.getRect().x - (b1.getRect().x + b1.getRect().w);
		xInvExit = (b2.getRect().x + b2.getRect().w) - b1.getRect().x;
	}
	else
	{
		xInvEntry = (b2.getRect().x + b2.getRect().w) - b1.getRect().x;
		xInvExit = b2.getRect().x - (b1.getRect().x + b1.getRect().w);
	}
	
	if (b1.getVelocity().y > 0.0f)
	{
		yInvEntry = b2.getRect().y - (b1.getRect().y + b1.getRect().h);
		yInvExit = (b2.getRect().y + b2.getRect().h) - b1.getRect().y;
	}
	else
	{
		yInvEntry = (b2.getRect().y + b2.getRect().h) - b1.getRect().y;
		yInvExit = b2.getRect().y - (b1.getRect().y + b1.getRect().h);
	}
	
	// find time of collision and time of leaving for each axis (if statement is to prevent divide by zero)
	float xEntry, yEntry;
	float xExit, yExit;
	
	if (b1.getVelocity().x == 0.0f)
	{
		xEntry = -std::numeric_limits<float>::infinity();
		xExit = std::numeric_limits<float>::infinity();
	}
	else
	{
		xEntry = xInvEntry / b1.getVelocity().x;
		xExit = xInvExit / b1.getVelocity().x;
	}
	
	if (b1.getVelocity().y == 0.0f)
	{
		yEntry = -std::numeric_limits<float>::infinity();
		yExit = std::numeric_limits<float>::infinity();
	}
	else
	{
		yEntry = yInvEntry / b1.getVelocity().y;
		yExit = yInvExit / b1.getVelocity().y;
	}
	
	// find the earliest/latest times of collision
	float entryTime = std::max(xEntry, yEntry);
	float exitTime = std::min(xExit, yExit);
	
	// if there was no collision
	if (entryTime > exitTime || (xEntry < 0.0f && yEntry < 0.0f) || xEntry > 1.0f || yEntry > 1.0f)
	{
		normalx = 0.0f;
		normaly = 0.0f;
		return 1.0f;
	}
	{
		// calculate normal of collided surface
		if (xEntry > yEntry)
		{
			if (xInvEntry < 0.0f)
			{
				normalx = 1.0f;
				normaly = 0.0f;
			}
			else
			{
				normalx = -1.0f;
				normaly = 0.0f;
			}
		}
		else
		{
			if (yInvEntry < 0.0f)
			{
				normalx = 0.0f;
				normaly = 1.0f;
			}
			else
			{
				normalx = 0.0f;
				normaly = -1.0f;
			}
		}
	}
	
	// return the time of collision
	return entryTime;
}


Entity* TestingScreen::GetSweptBroadphaseBox(Entity* b)
{
	Entity* broadphasebox = new Entity();
	broadphasebox->getRect().x = b->getVelocity().x > 0 ? b->getRect().x : b->getRect().x + b->getVelocity().x;
	
	broadphasebox->getRect().y = b->getVelocity().y > 0 ? b->getRect().y : b->getRect().y + b->getVelocity().y;
	
	broadphasebox->getRect().w = b->getVelocity().x > 0 ? b->getVelocity().x + b->getRect().w : b->getRect().w - b->getVelocity().x;
	
	broadphasebox->getRect().h = b->getVelocity().y > 0 ? b->getVelocity().y + b->getRect().h : b->getRect().h - b->getVelocity().y;
	
	
	return broadphasebox;
}


bool TestingScreen::AABBCheck(Entity* b1, Entity* b2)
{
	return !(b1->getRect().x + b1->getRect().w < b2->getRect().x || b1->getRect().x > b2->getRect().x + b2->getRect().w || b1->getRect().y + b1->getRect().h < b2->getRect().y || b1->getRect().y > b2->getRect().y + b2->getRect().h);
}


void TestingScreen::response()
{
	Entity* broadphasebox = GetSweptBroadphaseBox(&m_player);
	
	for (int i = 0; i < levelentities.size(); ++i)
	{
		
	
	if (AABBCheck(broadphasebox, &levelentities[i]))
	//if (AABBCheck(broadphasebox, &m_test))
	{
		float normalx, normaly;
		float collisiontime = SweptAABB(m_player, levelentities[i], normalx, normaly);
		
		m_player.getRect().x += m_player.getVelocity().x * collisiontime;
		m_player.getRect().y += m_player.getVelocity().y * collisiontime;
		
		if (collisiontime < 1.0f)
		{
			// perform response here
			//std::cout << "collision";
			float remainingtime = 1.0f - collisiontime;
			
			// slide
			float dotprod = (m_player.getVelocity().x * normaly + m_player.getVelocity().y * normalx) * remainingtime;
			glm::vec2 newVelocity;
			newVelocity.x = (dotprod * normaly);
			newVelocity.y = (dotprod * normalx);
			
			m_player.setVelocity(newVelocity);
		}
	}
	}
}


bool TestingScreen::collideInteractable()
{
	Entity* broadphasebox = GetSweptBroadphaseBox(&m_player);
	if (AABBCheck(broadphasebox, &object))
	{
		return true;
	}
	return false;
}
