#pragma once

#include <vector>

#include "ScreenManagerInterface.h"
#include "Renderer.hpp"
#include "Entity.hpp"

class ScreenManager;

class Screen
{
public:
	virtual ~Screen() {}

	virtual void update() = 0;
	virtual void render() = 0;
	
	std::vector<Entity>& getEntityVector()
	{
		return m_entityVector;
	}
		
protected:
	ScreenManagerInterface *m_screenManager;
	Renderer *m_renderer;
	std::vector<Entity> m_entityVector;
};

