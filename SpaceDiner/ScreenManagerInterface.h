//
//  ScreenManagerInterface.h
//  SpaceDiner
//
//  Created by Martin Grant on 17/12/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#ifndef ScreenManagerInterface_h
#define ScreenManagerInterface_h

#include <iostream>

class ScreenManagerInterface
{
public:
	virtual ~ScreenManagerInterface() { }
		
	virtual void update() = 0;
	virtual void render() = 0;
	
	virtual void changeScreen(std::string screen) = 0;
};


#endif /* ScreenManagerInterface_h */
