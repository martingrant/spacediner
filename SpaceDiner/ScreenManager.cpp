//
//  ScreenManager.cpp
//  SpaceDiner
//
//  Created by Martin Grant on 17/12/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#include "ScreenManager.hpp"

ScreenManager::ScreenManager(Renderer *renderer)
{
	m_currentScreen = nullptr;
	m_testingScreen = new TestingScreen(this, renderer);
	m_introScreen = new IntroScreen(this, renderer);
	m_mainMenuScreen = new MainMenuScreen(this, renderer);
	
	m_currentScreen = m_introScreen;
}


ScreenManager::~ScreenManager()
{
	
}


void ScreenManager::update()
{
	m_currentScreen->update();
}


void ScreenManager::render()
{
	m_currentScreen->render();
}


void ScreenManager::changeScreen(std::string screen)
{
	if (screen == "intro") m_currentScreen = m_introScreen;
	if (screen == "testing") m_currentScreen = m_testingScreen;
	if (screen == "mainmenu") m_currentScreen = m_mainMenuScreen;
}
