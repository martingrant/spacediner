//
//  Application.hpp
//  SpaceDiner
//
//  Created by Martin Grant on 29/11/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#ifndef Application_hpp
#define Application_hpp

#include <stdio.h>

#include "WindowManager.hpp"
#include "Input/SDLInput.h"
#include "Input/Input.h"
#include "TimeManager.h"

class Application
{
public:
	Application();
	~Application();
	
	bool run();
	
private:
	WindowManager* m_windowManager;
	
	Input *m_input;
	
	TimeManager *m_timeManager;
	
};

#endif /* Application_hpp */
