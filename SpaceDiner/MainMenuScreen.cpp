//
//  MainMenuScreen.cpp
//  SpaceDiner
//
//  Created by Martin Grant on 16/01/2017.
//  Copyright © 2017 Martin Grant. All rights reserved.
//

#include "MainMenuScreen.hpp"

MainMenuScreen::MainMenuScreen(ScreenManagerInterface *screenManager, Renderer *renderer)
{
	m_screenManager = screenManager;
	m_renderer = renderer;
}


MainMenuScreen::~MainMenuScreen()
{
	
}


void MainMenuScreen::update()
{
	static int change = 0;
	change++;
	
	if (change > 40)
		m_screenManager->changeScreen("testing");
}


void MainMenuScreen::render()
{
	m_renderer->renderText(250, 250, "main menu", 100);
}
