//
//  LevelNode.cpp
//  SpaceDiner
//
//  Created by Martin Grant on 08/01/2017.
//  Copyright © 2017 Martin Grant. All rights reserved.
//

#include "LevelNode.hpp"

LevelNode::LevelNode(int x, int y, int type, bool traversible)
{
	m_gridPosition.first = x;
	m_gridPosition.second = y;
	
	m_type = type;
	
	m_traversible = traversible;
	
	
	switch (type)
	{
		case 0:
			m_texture = "tile";
			break;
		case 9:
			m_texture = "wall";
			break;
		default:
			break;
	}
}


LevelNode::~LevelNode()
{
	
}


std::pair<int, int> LevelNode::getGridPosition()
{
	return m_gridPosition;
}


std::string LevelNode::getTexture()
{
	return m_texture;
}


bool LevelNode::isTraversible()
{
	return m_traversible;
}


int LevelNode::getType()
{
	return m_type;
}
