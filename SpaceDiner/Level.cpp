//
//  Level.cpp
//  SpaceDiner
//
//  Created by Martin Grant on 08/00/2007.
//  Copyright © 2007 Martin Grant. All rights reserved.
//

#include "Level.hpp"

Level::Level(int width, int height) : m_levelWidth(width), m_levelHeight(height)
{
	std::vector<std::vector<int>> map
	{
		{9,9,9,9,9,9,9,9,9,9,9,9,9},
		{9,0,0,0,0,9,0,0,0,0,0,0,9},
		{9,0,9,9,0,9,0,0,0,9,0,0,9},
		{9,0,9,0,0,9,0,0,0,0,0,0,9},
		{9,0,9,0,9,0,9,0,0,9,9,0,9},
		{9,0,0,0,0,0,9,0,0,0,9,0,9},
		{9,0,9,0,9,9,9,0,0,0,9,0,9},
		{9,0,9,0,0,0,9,9,0,0,0,0,9},
		{9,0,9,0,0,0,9,0,0,0,9,0,9},
		{9,0,9,0,0,0,0,0,0,0,9,0,9},
		{9,0,9,9,9,9,0,9,9,9,9,0,9},
		{9,0,0,0,0,0,0,0,9,0,0,0,9},
		{9,9,9,9,9,9,9,9,9,9,9,9,9},
	};
	
	
	m_level = std::vector<std::vector<LevelNode>>(m_levelWidth, std::vector<LevelNode>(m_levelHeight));
	
	for (int i = 0; i < m_levelWidth; ++i)
	{
		for (int j = 0; j < m_levelHeight; ++j)
		{
			m_level[i][j] = LevelNode(0, 0, map[i][j], true);
			
			m_levelVector.push_back(map[i][j]);
		}
	}
}


Level::~Level()
{
	
}


int Level::getWidth()
{
	return m_levelWidth;
}


int Level::getHeight()
{
	return m_levelHeight;
}


std::vector<std::vector<LevelNode>> Level::getLevel()
{
	return m_level;
}


std::vector<int> Level::getLevelVector()
{
	return m_levelVector;
}


std::vector<std::pair<int, int>> Level::findPath()
{
	srand (time(NULL));
	
	bool pathfound = false;
	
	while (pathfound == false)
	{
		
		int x = rand() % 13 + 2;
		int y = rand() % 13 + 2;
		
		if (m_levelVector[(x * 13) + y] == 0)
		{
			return(findPathTest(1, 1, x, y));
			pathfound = true;
			
			break;
		}
	}
	return std::vector<std::pair<int, int>>();
}
