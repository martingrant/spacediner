//
//  main.cpp
//  SDLTest
//
//  Created by Martin Grant on 29/11/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#include <iostream>

#include <SDL2/SDL.h>

#include "Application.hpp"

bool running = true;

int main(int argc, const char * argv[]) {
	
	Application* application = new Application();
	
	while (application->run() == true)
		continue;

    return 0;
}
