//
//  Entity.hpp
//  SpaceDiner
//
//  Created by Martin Grant on 04/12/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#ifndef Entity_hpp
#define Entity_hpp

#include <stdio.h>
#include <string>
#include <iostream>

#include <SDL2/SDL.h>
#include "glm/glm.hpp"

#include "Input/Input.h"
#include "Input/SDLInput.h"

class Entity
{
public:
	Entity() { }
	Entity(std::string texture, float x, float y, float width, float height, bool isPlayer, bool interact);
	~Entity();
	
	SDL_Rect &getRect();
	
	void moveUp();
	void moveDown();
	void moveLeft();
	void moveRight();
	
	void moveTo(float x, float y);
	void moveToUpdate();
	void setpath(std::vector<std::pair<int,int>> newpath);
	
	void pathupdate();
	
	void update();
	
	void setVelocity(glm::vec2 velocity);
	glm::vec2 getVelocity();
	
	bool hasInteract();
	float getInteractBarWidth();
	void setInteracting(bool toggle);
	
	std::string getTextureName();
	
	bool isMoving();
	
private:
	std::string m_texture;
	float m_positionX;
	float m_positionY;
	float m_width;
	float m_height;
	
	SDL_Rect m_rect;
	
	Input *m_input;
	
	float m_acceleration;
	glm::vec2 m_velocity;
	
	bool m_isPlayer;
	
	bool m_interact;
	float m_interactBarWidth;
	bool m_interacting;
	
	bool m_isMoving;
	glm::vec2 direction;
	glm::vec2 start;
	glm::vec2 end;
	float distance;
	
	
	std::vector<std::pair<int,int>> m_path;
};

#endif /* Entity_hpp */
