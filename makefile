CC = g++

INCLUDES = -I /usr/local/include/

COMPILER_FLAGS = -std=c++11 -c -Wall $(INCLUDES)

LDFLAGS = -L/usr/local/lib -lSDL2 -lSDL2_image -lSDL2_ttf

SOURCES = $(wildcard *.cpp) $(wildcard **/*.cpp)

OBJECTS = $(SOURCES:.cpp=.o)

EXECUTABLE = spacediner

all: $(EXECUTABLE) $(SOURCES) $(EXECUTABLE)
    
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o SpaceDiner/$@

.cpp.o:
	$(CC) $(COMPILER_FLAGS) $< -o $@

clean:
	$(RM) -f SpaceDiner/$(EXECUTABLE) *.o
