//
//  Pathfind.h
//  SpaceDiner
//
//  Created by Martin Grant on 30/12/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#ifndef Pathfind_h
#define Pathfind_h

#include <vector>
#include <utility>

const int MAP_WIDTH = 13;
const int MAP_HEIGHT = 13;

extern int world_map[MAP_WIDTH * MAP_HEIGHT];

std::vector<std::pair<int,int>> findPathTest(int startX, int startY, int endX, int endY);

class PathNode
{
public:
	PathNode();
	~PathNode();
	
	int x;
	int y;
	int type;
	
private:
};

#endif /* Pathfind_h */
