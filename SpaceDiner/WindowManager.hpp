//
//  WindowManager.hpp
//  SpaceDiner
//
//  Created by Martin Grant on 29/11/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#ifndef WindowManager_hpp
#define WindowManager_hpp

#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include "Input/Input.h"
#include "Input/SDLInput.h"
#include "Renderer.hpp"
#include "ScreenManager.hpp"

class WindowManager
{
public:
	WindowManager(std::string windowTitle, int windowWidth, int windowHeight);
	~WindowManager();
	
	bool update();
	void render();
	
	SDL_Window *getWindow();
	
	void createWindow();
	void setWindowTitle(std::string title);
	void setWindowSize(int width, int height);
	void setWindowWidth(int width);
	void setWindowHeight(int height);
	void minimiseWindow();
	void restoreWindow();
	void maximiseWindow();
	void setFullScreenMode();
	void setWindowedMode();
	
private:
	SDL_Window *m_window;
	SDL_Event m_events;
	
	bool m_running;
	
	std::string m_windowTitle;
	int m_windowWidth;
	int m_windowHeight;
	
	Renderer *m_renderer;
	Input *m_input;
	
	ScreenManager *m_screenManager;
	
};

#endif /* WindowManager_hpp */
