//
//  MainMenuScreen.hpp
//  SpaceDiner
//
//  Created by Martin Grant on 16/01/2017.
//  Copyright © 2017 Martin Grant. All rights reserved.
//

#ifndef MainMenuScreen_hpp
#define MainMenuScreen_hpp

#include <stdio.h>

#include "Screen.h"

class MainMenuScreen : public Screen
{
public:
	MainMenuScreen(ScreenManagerInterface *screenManager, Renderer *renderer);
	virtual ~MainMenuScreen();
	
	virtual void update();
	virtual void render();
	
private:
	
};

#endif /* MainMenuScreen_hpp */
