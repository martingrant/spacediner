//
//  Application.cpp
//  SpaceDiner
//
//  Created by Martin Grant on 29/11/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#include "Application.hpp"

Application::Application()
{
	m_windowManager = new WindowManager("SpaceDiner", 800, 600);
	m_input = new SDLInput(0);
	m_timeManager = new TimeManager(30, 5);
}


Application::~Application()
{
	delete m_windowManager;
}


bool Application::run()
{
	bool status = true;
	
	m_timeManager->resetNumberOfLoops();
	
	while (m_timeManager->getTickCount() > m_timeManager->getNextGameTick() && m_timeManager->getNumberOfLoops() < m_timeManager->getMaxFrameSkip())
	{
		status = m_windowManager->update();
		
		m_timeManager->incrementNextGameTick();
		m_timeManager->incrementNumberOfLoops();
	}
	
	m_timeManager->setInterpolation();
	
	
	m_windowManager->render();
	
	return status;
}
