//
//  Renderer.hpp
//  SpaceDiner
//
//  Created by Martin Grant on 04/12/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#ifndef Renderer_hpp
#define Renderer_hpp

#include <stdio.h>
#include <vector>
#include <iostream>
#include <unordered_map>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_Image.h>

#include "Entity.hpp"

class Renderer
{
public:
	Renderer(SDL_Window *window);
	~Renderer();
	
	void renderBegin();
	void renderEnd();
	void render(Entity &entity);
	void render(std::string texture, int x, int y, int width, int height);
	void renderTest(int x, int y, int type, std::string texture);
	void renderBar(int x, int y, int width, int height);
	void renderText(int x, int y, std::string text, int alpha);
	
private:
	SDL_Renderer *m_renderer;
	
	SDL_Texture *bitmapTex;
	SDL_Texture *bitmapTex2;
	
	std::unordered_map<std::string, SDL_Texture*> m_textureMap;
	
	TTF_Font *font;
	
};

#endif /* Renderer_hpp */
