//
//  IntroScreen.h
//  SpaceDiner
//
//  Created by Martin Grant on 18/12/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#ifndef IntroScreen_h
#define IntroScreen_h

#include "Screen.h"

class IntroScreen : public Screen
{
public:
	IntroScreen(ScreenManagerInterface *screenManager, Renderer *renderer);
	virtual ~IntroScreen();
	
	virtual void update();
	virtual void render();
	
private:
	
};

#endif /* IntroScreen_h */
