#pragma once

#include <SDL2/SDL.h>

#include "Screen.h"
#include "Renderer.hpp"
#include "Entity.hpp"
#include "Level.hpp"

class TestingScreen : public Screen
{
public:
	TestingScreen(ScreenManagerInterface *screenManager, Renderer *renderer);
	~TestingScreen();

	virtual void update();
	virtual void render();
	
	float SweptAABB(Entity b1, Entity b2, float& normalx, float& normaly);
	Entity* GetSweptBroadphaseBox(Entity* b);
	bool AABBCheck(Entity* b1, Entity* b2);
	void response();
	
	bool collideInteractable();

private:	
	Entity m_player;
	Entity m_test;
	Entity object;
	
	Input *m_input;
	
	std::vector<std::pair<int,int>> path;
	
	Level* m_level;
	
	std::vector<Entity> levelentities;
};
