//
//  WindowManager.cpp
//  SpaceDiner
//
//  Created by Martin Grant on 29/11/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#include "WindowManager.hpp"

WindowManager::WindowManager(std::string windowTitle, int windowWidth, int windowHeight) : m_windowTitle(windowTitle), m_windowWidth(windowWidth), m_windowHeight(windowHeight)
{
	m_running = true;
	
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	
	m_window = SDL_CreateWindow(m_windowTitle.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_windowWidth, m_windowHeight, SDL_WINDOW_SHOWN);
	if (m_window == NULL)
	{
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
	}
	
	m_renderer = new Renderer(m_window);
	m_input = new SDLInput(0);
	
	m_screenManager = new ScreenManager(m_renderer);
}


WindowManager::~WindowManager()
{
	SDL_DestroyWindow(m_window);
	SDL_Quit();
	
	delete m_renderer;
	delete m_input;
}


bool WindowManager::update()
{
	m_input->update();
	
	while (SDL_PollEvent(&m_events))
	{
		switch (m_events.type)
		{
			case SDL_WINDOWEVENT:
				switch (m_events.window.event)
			{
				case SDL_WINDOWEVENT_CLOSE:
					m_running = false;
					break;
			}
				break;
			case SDL_QUIT:
				m_running = false;
				break;
		}
	}
	
	m_screenManager->update();
	
	if (m_input->getKeyState("t")) m_screenManager->changeScreen("testing");
	
	return m_running;
}


void WindowManager::render()
{
	m_renderer->renderBegin();
	
	m_screenManager->render();
	
	m_renderer->renderEnd();
}


SDL_Window* WindowManager::getWindow()
{
	return m_window;
}


void WindowManager::setWindowTitle(std::string title)
{
	m_windowTitle = title;
	SDL_SetWindowTitle(m_window, m_windowTitle.c_str());
}


void WindowManager::setWindowSize(int width, int height)
{
	m_windowWidth = width;
	m_windowHeight = height;
	SDL_SetWindowSize(m_window, m_windowWidth, m_windowHeight);
}


void WindowManager::setWindowWidth(int width)
{
	m_windowWidth = width;
	SDL_SetWindowSize(m_window, m_windowWidth, m_windowHeight);
}


void WindowManager::setWindowHeight(int height)
{
	m_windowHeight = height;
	SDL_SetWindowSize(m_window, m_windowWidth, m_windowHeight);
}


void WindowManager::minimiseWindow()
{
	SDL_MinimizeWindow(m_window);
}


void WindowManager::restoreWindow()
{
	SDL_RestoreWindow(m_window);
}


void WindowManager::maximiseWindow()
{
	SDL_MaximizeWindow(m_window);
}


void WindowManager::setFullScreenMode()
{
	SDL_SetWindowFullscreen(m_window, SDL_WINDOW_FULLSCREEN);
}


void WindowManager::setWindowedMode()
{
	SDL_SetWindowFullscreen(m_window, 0);
}
