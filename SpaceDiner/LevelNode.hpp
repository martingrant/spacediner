//
//  LevelNode.hpp
//  SpaceDiner
//
//  Created by Martin Grant on 08/01/2017.
//  Copyright © 2017 Martin Grant. All rights reserved.
//

#ifndef LevelNode_hpp
#define LevelNode_hpp

#include <stdio.h>
#include <utility>
#include <string>

enum type {
	FLOOR = 0,
	WALL = 9,
};

class LevelNode
{
public:
	LevelNode() { }
	LevelNode(int t) { m_type = t; }
	LevelNode(int x, int y, int type, bool traversible);
	~LevelNode();
	
	std::pair<int, int> getGridPosition();
	std::string getTexture();
	bool isTraversible();
	int getType();
	
private:
	std::pair<int, int> m_gridPosition;
	std::string m_texture;
	bool m_traversible;
	int m_type;
};

#endif /* LevelNode_hpp */
