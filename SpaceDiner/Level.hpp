//
//  Level.hpp
//  SpaceDiner
//
//  Created by Martin Grant on 08/01/2017.
//  Copyright © 2017 Martin Grant. All rights reserved.
//

#ifndef Level_hpp
#define Level_hpp

#include <iostream>
#include <vector>

#include "LevelNode.hpp"
#include "Pathfinding/Pathfind.h"

class Level
{
public:
	Level(int width, int height);
	~Level();
	
	int getWidth();
	int getHeight();
	std::vector<std::vector<LevelNode>> getLevel();
	std::vector<int> getLevelVector();
	
	std::vector<std::pair<int, int>> findPath();
	
	
private:
	int m_levelWidth;
	int m_levelHeight;
	std::vector<std::vector<LevelNode>> m_level;
	std::vector<int> m_levelVector;
};

#endif /* Level_hpp */
