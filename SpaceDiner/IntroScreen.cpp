//
//  IntroScreen.cpp
//  SpaceDiner
//
//  Created by Martin Grant on 16/01/2017.
//  Copyright © 2017 Martin Grant. All rights reserved.
//

#include "IntroScreen.h"

IntroScreen::IntroScreen(ScreenManagerInterface *screenManager, Renderer *renderer)
{
	m_screenManager = screenManager;
	m_renderer = renderer;
}


IntroScreen::~IntroScreen()
{
	
}


void IntroScreen::update()
{
	
}


void IntroScreen::render()
{
	static float alpha = 0;
	alpha += 0.125;
	
	
	if (alpha == 255)
		m_screenManager->changeScreen("mainmenu");
	
	m_renderer->renderText(235, 200, "slim's space diner", (int)alpha);
}
