//
//  Entity.cpp
//  SpaceDiner
//
//  Created by Martin Grant on 04/12/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#include "Entity.hpp"

Entity::Entity(std::string texture, float x, float y, float width, float height, bool isPlayer, bool interact) : m_texture(texture), m_positionX(x), m_positionY(y), m_width(width), m_height(height), m_isPlayer(isPlayer), m_interact(interact), m_interactBarWidth(m_width)
{
	m_rect.x = m_positionX;
	m_rect.y = m_positionY;
	m_rect.w = m_width;
	m_rect.h = m_height;
	
	m_velocity = glm::vec2(0.0f);
	m_acceleration = 2.0f;
	
	m_input = new SDLInput(0);
	
	m_isMoving = false;
	direction = glm::vec2(0.0f);
}


Entity::~Entity()
{
	//delete m_input;
}


SDL_Rect& Entity::getRect()
{
	return m_rect;
}


void Entity::moveUp()
{
	m_velocity.y -= m_acceleration;
}


void Entity::moveDown()
{
	m_velocity.y += m_acceleration;
}


void Entity::moveLeft()
{
	m_velocity.x -= m_acceleration;
}


void Entity::moveRight()
{
	m_velocity.x += m_acceleration;
}


void Entity::moveTo(float x, float y)
{
	start.x = m_rect.x;
	start.y = m_rect.y;
	
	end.x = x;
	end.y = y;
	
	distance = glm::distance(start, end);
	direction = glm::normalize(end - start);
	m_rect.x = start.x;
	m_rect.y = start.y;

	m_isMoving = true;
}


void Entity::moveToUpdate()
{
	if (m_isMoving)
	{
		m_rect.x += direction.x * 4;
		m_rect.y += direction.y * 4;
		
		glm::vec2 pos;
		pos.x = m_rect.x;
		pos.y = m_rect.y;
		if (glm::distance(start, pos) >= distance)
		{
			m_rect.x = end.x;
			m_rect.y = end.y;
			m_isMoving = false;
		}
	}
}


void Entity::setpath(std::vector<std::pair<int,int>> newpath)
{
	m_path = newpath;
}


void Entity::pathupdate()
{
	// if path is not empty, we haven't moved along it to the end
	// move to the node at the start of the path, and then remove it
	// may be better inserting into vector in reverse order and removing from the back
	
	if (m_path.empty() == false)
	{
		if (m_isMoving == false)
		{
			int x = m_path.front().first * 40;
			int y = m_path.front().second * 40;
			
			moveTo(x, y);
			
			m_path.erase(m_path.begin());
		}
	}
}


void Entity::update()
{
	pathupdate();
	moveToUpdate();
	
	if (m_isPlayer == true)
	{
		m_input->update();
		
		m_rect.x += m_velocity.x;
		m_rect.y += m_velocity.y;
		
		
		if (m_input->getKeyState("W"))
		{
			moveUp();
		}
		if (m_input->getKeyState("S"))
		{
			moveDown();
		}
		if (m_input->getKeyState("A"))
		{
			moveLeft();
		}
		if (m_input->getKeyState("D"))
		{
			moveRight();
		}
		
		
		m_velocity *= 0.9f;
		
		if (glm::length(m_velocity) < 0.001f)
		{
			m_velocity = glm::vec2(0.0f);
		}
	}
	
	
	if (m_interact == true)
	{
		if (m_interacting == true)
		{
			if (m_interactBarWidth < 101)
			{
				m_interactBarWidth += 1.0f;
			}
		}
		else
		{
			if (m_interactBarWidth >= 0.0f)
			{
				m_interactBarWidth -= 1.0f;
			}
		}
	}
}


void Entity::setVelocity(glm::vec2 velocity)
{
	m_velocity = velocity;
}


glm::vec2 Entity::getVelocity()
{
	return m_velocity;
}


bool Entity::hasInteract()
{
	return m_interact;
}


float Entity::getInteractBarWidth()
{
	return m_interactBarWidth;
}


void Entity::setInteracting(bool toggle)
{
	m_interacting = toggle;
}


std::string Entity::getTextureName()
{
	return m_texture;
}


bool Entity::isMoving()
{
	return m_isMoving;
}
