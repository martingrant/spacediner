//
//  ScreenManager.hpp
//  SpaceDiner
//
//  Created by Martin Grant on 17/12/2016.
//  Copyright © 2016 Martin Grant. All rights reserved.
//

#ifndef ScreenManager_hpp
#define ScreenManager_hpp

#include <stdio.h>

#include "Screen.h"
#include "ScreenManagerInterface.h"
#include "TestingScreen.h"
#include "IntroScreen.h"
#include "MainMenuScreen.hpp"

class ScreenManager : ScreenManagerInterface
{
public:
	ScreenManager(Renderer *renderer);
	~ScreenManager();
	
	virtual void update();
	virtual void render();
	
	virtual void changeScreen(std::string screen);
	
private:	
	Screen *m_currentScreen;
	Screen *m_testingScreen;
	Screen *m_introScreen;
	Screen *m_mainMenuScreen;
	
};

#endif /* ScreenManager_hpp */
